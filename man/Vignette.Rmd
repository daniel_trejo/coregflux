---
title: "CoRegFlux vignette"
author: "Daniel Trejo Banos"
date: "5 Dec 2016"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## CoRegFlux vignette


```{r,eval=F}
library(CoRegFlux)
```

Initial conditions

We initialize a metabolite data frame, using adalab conditions. Ethanol is set to 0.01 (we assume it's initial concentration should be close to 0).

We also load the first batch of OD measurements from Adalab.

```{r,warning=F,error=F,message=F}

library(CoRegFlux)
library(sybil)
library(sybilEFBA)
data("iMM904")
 
 
 iMM904 <- upgradeModelorg(iMM904) 
 ## Data frame for metabolites
 
name=c("glucose","ethanol")
 concentrations=c(13.8768699776138,0.01)
metabolites<-data.frame(name,concentrations)
 
 ## we read the initial biomass from adalab od readings
data("All_Wild_Type_GrowthCurves")
#The following line is to only pick reasings below 24 hours 
#All_Wild_Type_GrowthCurves<- All_Wild_Type_GrowthCurves[All_Wild_Type_GrowthCurves$X<24,]
time<-All_Wild_Type_GrowthCurves$Time
Tfinal<-time[length(time)]

 OD<-as.matrix(All_Wild_Type_GrowthCurves[,-1])
 OD<-as.matrix(All_Wild_Type_GrowthCurves[,-1])
 rownames(OD)<-time

 OD<-t(OD)
 #in case you want to normalize the OD
 #mOD <- max(OD)
 #OD<-OD/mOD
 meanOD<-colMeans(OD)

 propConst <- 0.45
 initial.biomass <- propConst*meanOD[1]
print(initial.biomass)
print(time)
```





Here we create a data frame with the name of the metabolic genes and their state, all genomic state data frames should follow the same format.
```{r}
gene.state<-data.frame(Name=c("NTE1","TYR1"),State=c(0,1))
```

The simulation receives as parameter the gene.state function, this function will receive as parameters a gene.state and metabolite state(in the same format as the data frame previously used). in this example the gene state function just returns the value of the gene.state variable created in the previous chunk.

```{r,warning=F,error=F,message=F}
result<-Simulation(model=iMM904,
                   as.numeric(time),
                   metabolites,
                   initial.biomass,
                   coregnet=NULL,
                   knock.out=NULL,
                   influence=NULL,
                   gene.state.function=function(a,b){gene.state})
```

The additional parameters coregnet, knock.out and influence are used if you want to perform  a simulation using a given coregnet, with a single knock out given by the variable knock.out. The influence parameter is a real and is the value to be used by the logistic function to readjust the weights.

Right now the simulations can be performed with or without a coregnet and with or without a gene.state function (the GRN simulator)

If you wish more control you can run a single simulation step.
  
```{r,eval=FALSE}
Simulation.Step (model,
                 coregnet,
                 metabolites,
                 metabolites.concentrations.t0,
                 biomass.t0,
                 TF,
                 influence,
                 time.step,
                 gene.state)
```

a single simulation step receives a metabolic model and performs:
  - update fluxes by metabolites concentrations
  
  - update fluxes by coregnet and influence value
  
  - update fluxes by gene state from the GRN simulator


The result is a list containing
  
  - objective.history: time series of objective function value for the linear program 
  
  - metabolites: metabolites concentrations in the last time step
  
  - fluxes.history: time series of the fluxes values for all the time series
  
  - metabolites.concentration.history: time series of metabolite concentrations 
  
  - metabolites.fluxes.history: time series of the metabolites fluxes during all the simulation
  
  - rate.history: time series of the growth rate values for all simulation
  
  - time: vector containing the simulation times
  
  - gene.state.history: list containing the values for the gene state during all the simulation.
```{r,eval=FALSE}
result<-list(objective.history = objective.history,
               metabolites= step.results$metabolites,
               fluxes.history = fluxes.history,
               metabolites.cocentration.history = metabolites.conentration.history,
               metabolites.fluxes.history = metabolites.fluxes.history,
               rate.history = rate.history,biomass.history = biomass.history,
               time = time,
               gene.state.history=gene.state.history)
```

For example lets plot the resulting biomass

```{r}
plot(result$biomass.history)
```

Lets try without gene state

```{r}
result<-Simulation(model=iMM904,
                   as.numeric(time),
                   metabolites,
                   initial.biomass,
                   coregnet=NULL,
                   knock.out=NULL,
                   influence=NULL,
                   gene.state.function=NULL)
plot(result$biomass.history)
```

Now lets try using a coregnet network

```{r}
library(CoRegNet)
data("refined.nov14")
data("InfluencesMay11")


geneName<-"XBP1"
knock.out<- data.frame(geneName)

influence<-BrauerInf[match(geneName,rownames(BrauerInf)), ]
influence<-mean(influence[1:7])

result <- Simulation(model = iMM904,metabolites = metabolites,time = time,initial.biomass = initial.biomass,coregnet = refined0_5 ,knock.out = knock.out,influence = influence)
plot(result$biomass.history)

```

